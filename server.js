console.log("Server is starting");

var express = require('express');

var app = express();

var server = app.listen(3000);

app.use(express.static('public'));

app.get('/asdf', sayHello);
function sayHello(request, response) {
  response.send("Hello!");
}


app.get('/search/:animal', searchAnimal);
function searchAnimal(request, response) {
  var data = request.params;
  response.send("Looks like you like " + data.animal + "!");
}


var simple_kg = {
	"DIS005000": "COVID-19",
	"DRU003422": "Decitabine",
	"DRU007981": "Remdesivir",
	"FIR000283": "Genentech",
	"FIR000025": "Amgen"
}

app.get('/view_terms', viewKG);
function viewKG(request, response) {
	response.send(simple_kg);
}


app.get('/add_term/:source_id/:term', addToKG);
function addToKG(request, response) {
	var data = request.params;
	simple_kg[data.source_id] = data.term
	var reply = {
		message: "Thank you for adding a new term!"
	}
	response.send(reply);
}


// let express = require('express');
// let app = express();
// var server = app.listen(3000);
// // For POST-Support
// let bodyParser = require('body-parser');
// let multer = require('multer');
// let upload = multer();

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));

// app.post('/login', loginUsingData);

// function loginUsingData(req, res) {
//   console.log(req.body);
//   var name = req.body.name;
//   var password = req.body.password;
//   var reply = {
//     message: "thank you"
//   }
//   res.send(reply);
// }
