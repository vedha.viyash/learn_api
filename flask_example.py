from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route("/aaaa", methods=["GET"])
def aaaa():
    return "<html>Hello from <b>python</b>!</html>"

@app.route("/asdf", methods=["GET"])
def asdf():
    return "Hello from python!"

@app.route("/qwerty", methods=["GET"])
def qwerty():
    return jsonify({"message": "Hello from python!"})

@app.route('/qwerty/<argument>', methods=["GET"])
def qwertyarg(argument):
  return jsonify({"you sent": argument})

@app.route('/qwerty_post', methods=["POST"])
def qwerty_post():
  page_number = request.args.get('page', default = 0, type = int)
  filter_value = request.args.get('filter', default = "", type = str)
  return {"page": page_number, "filter": filter_value}

@app.route('/qwerty_any', methods=["POST", "GET"])
def qwerty_any():
  page_number = request.args.get('page', default = 0, type = int)
  filter_value = request.args.get('filter', default = "", type = str)
  return {"page": page_number, "filter": filter_value}

if __name__ == "__main__":
    app.run()
