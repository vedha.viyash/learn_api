library(shiny)

ui <- fluidPage(
    fluidRow(
        column(
            3,
            selectInput(
                "cities", "Cities",
                c("Boston", "Chennai", "New york", "San Diego", "San Francisco"),
                "Chennai", multiple = TRUE)),
        column(
            3,
            selectInput(
                "map_type", "Map type",
                c("OpenStreetMap", "Stamen", "Esri", "Esri.WorldTopoMap", "Esri.WorldImagery",
                "CartoDB", "CartoDB.DarkMatter", "CartoDB.Voyager")
            )
        )
    ),
    fluidRow(uiOutput("leaflet_plot", style = "height: 80vh;"))
)

server <- function(input, output, session) {
    output$leaflet_plot <- renderUI({
        cities <- paste(tolower(input$cities), collapse = ":")
        tags$iframe(
            src = paste0("http://127.0.0.1:8000/leaflet?cities=", cities, "&layout=", input$map_type),
            width = "100%", height = "100%"
        )
    })
}

shinyApp(ui, server)
